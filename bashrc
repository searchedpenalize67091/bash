#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '

alias pac=paru
alias sudo='sudo -v; sudo '
#alias paru=paru --clonedir "$XDG_DOWNLOAD_DIR"
